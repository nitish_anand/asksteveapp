# Intro
Assessment task for AskSteve React Native Developer application.

# Steps to install
 1. Clone the repo or download the zip
 2. Open terminal and switch to the directory
 3. Run `npm install`
 4. Run `react-native run-android` or `react-native run-ios` to run it on android or ios respectively.

Please make sure your device is connected or you have an emulator running.

# Screen Flow
![enter image description here](https://lh3.googleusercontent.com/w3f74oYsh5ch_PCrUIDXi-Smgt6Em6jxGv-lSklb4EfXXrWf5naFovaAeWiC2gNM_pzdeeOWPMY4 "username input")
![
](https://lh3.googleusercontent.com/iJ1QxT-wROkbfbBKd86D6R8JRas8u3dJABiDUcGq9l3Mo6-GHjx4c72j5aq9BDwLcKdT3Gxc7hsG "password input")
![
](https://lh3.googleusercontent.com/rQVnKlP-PXE5w3fdOqvnmagH4X6JDXp8FxlnsNDDubIUejd1gZthlGHaOgGvD-0WxuZW-yOnWLTN "repo input")
![
](https://lh3.googleusercontent.com/ML2C7932ifktE9LyyrSHt2RKsuX3-H_H1Pt0vogzud9UWNo3C1ASQnkIh6r82D_AI3OuphbCAP1v "commit listing")
