/* @flow weak */

import React from "react";
import { View, Text, StyleSheet, Image} from "react-native";
import theme from "../themes";

const ListItem = props => (
  <View style={[styles.listItem]}>
    <View style={styles.commiterInfo}>
      <View style={styles.avatarContainer}>
        <Image source={{ uri: props.avatar || '' }} style={{width: 50, height: 50}} />
      </View>
      <View>
        <Text style={styles.itemText}>{props.name}</Text>
        <Text style={styles.itemText}>{props.date}</Text>
      </View>
    </View>
    <Text style={styles.itemText}>{props.message}</Text>
  </View>
);

export { ListItem };

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    borderColor: "white",
    borderRadius: 4,
    borderWidth: 2
  },
  listItem: {
    flex: 1,
    borderColor: "white",
    borderRadius: 4,
    borderWidth: 2,
    marginBottom: 5,
    width: '100%',
    padding: 10,
    alignSelf: 'center'
  },
  avatarContainer: {
    width: 70,
    height: 70,
    backgroundColor: 'white',
    borderRadius: 35,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 20
  },
  itemText: {
    fontFamily: theme.fonts.regular,
    fontSize: 16,
    color: 'white'
  },
  commiterInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'white',
    paddingBottom: 20,
    marginBottom: 20
  }
});
