
/* @flow weak */

import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Text, StyleSheet, TouchableWithoutFeedback } from "react-native";
import theme from "../themes";
import { logoutUser } from '../actions';

class Header extends Component {

  onLogoutPress() {
    this.props.logoutUser();
  };

  render() {
    return (
      <View style={styles.header}>
        <TouchableWithoutFeedback onPress={() => this.onLogoutPress()}>
          <View>
            <Text style={[theme.styles.buttonText, { fontSize: 12 }]}>LOGOUT</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    zIndex: 1,
    width: '100%',
    height: 40,
    paddingRight: 20,
    paddingTop: 10,
    alignItems: 'flex-end',
    textAlign: 'right'
  },
});

const mapStateToProps = ({ repo }) => {
  return {};
};

export default connect(
  mapStateToProps,
  { logoutUser }
)(Header);
