/* @flow weak */

import React from 'react';
import { View, Text, StyleSheet, TextInput } from "react-native";
import theme from '../themes';

const Input = props => (
  <TextInput
      underlineColorAndroid="transparent"
      placeholder={props.placeholder}
      style={[styles.input, props.style]}
      value={props.value}
      onChangeText={props.onChangeText}
      autoCapitalize={props.autoCapitalize}
      secureTextEntry={props.secureTextEntry}
    />
);

export { Input };

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 25,
    height: 50,
    paddingLeft: 20,
    width: 300,
    alignSelf: 'center',
    fontFamily: theme.fonts.bold,
    marginBottom: 10
  },
});
