import {
  UPDATE_REPO_FORM,
  LIST_REPO_COMMITS,
  LIST_REPO_COMMITS_SUCCESS,
  LIST_REPO_COMMITS_FAIL
} from "../actions/types";

const INITIAL_STATE = {
  name: "facebook/react-native",
  loading: false,
  page: 1,
  last: 1,
  next: 1,
  commits: [],
  loadCommitFail: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_REPO_FORM:
      return {
        ...state,
        [action.payload.prop]: action.payload.value,
      }
    case LIST_REPO_COMMITS:
      return {
        ...state,
        loading: true
      }
    case LIST_REPO_COMMITS_SUCCESS:
      return {
        ...state,
        loading: false,
        commits: [...state.commits, ...action.payload.commits],
        last: action.payload.last,
        page: action.payload.page
      }
    case LIST_REPO_COMMITS_FAIL:
      return {
        ...state,
        loading: false,
        loadCommitFail: true
      }
    default:
      return state;
  }
};
