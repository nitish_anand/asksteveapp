import { combineReducers } from "redux";
import AuthReducer from "./AuthReducer";
import RepoReducer from "./RepoReducer";

const appReducer = combineReducers({
  auth: AuthReducer,
  repo: RepoReducer
});

export default appReducer;
