import {
  SHOW_PASSWORD_FIELD,
  UPDATE_AUTH_FORM,
  AUTHENTICATING_GITHUB,
  HIDE_PASSWORD_FIELD,
  AUTHENTICATION_GITHUB_FAIL,
  LOGOUT_USER
} from "../actions/types";

const INITIAL_STATE = {
  showPasswordField: false,
  username: null,
  password: null,
  loading: false,
  authFail: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_AUTH_FORM:
      return {
        ...state,
        [action.payload.prop]: action.payload.value,
      }
    case SHOW_PASSWORD_FIELD:
      return {
        ...state,
        showPasswordField: true
      }
    case HIDE_PASSWORD_FIELD:
      return {
        ...state,
        ...INITIAL_STATE
      }
    case AUTHENTICATING_GITHUB:
      return {
        ...state,
        loading: true
      }
    case AUTHENTICATION_GITHUB_FAIL:
      return {
        ...state,
        loading: false,
        authFail: true
      }
    case LOGOUT_USER:
      return {
        ...INITIAL_STATE
      }
    default:
      return state;
  }
};
