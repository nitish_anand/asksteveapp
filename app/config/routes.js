import React, { Component } from 'react';
import { Scene, Stack, Router, Actions } from 'react-native-router-flux';
import { BackHandler } from 'react-native';
import LoginScreen from '../screens/LoginScreen';
import RepoSelectionScreen from '../screens/RepoSelectionScreen';
import CommitListingScreen from '../screens/CommitListingScreen';

class RouterComponent extends Component {


  _backAndroidHandler = () => {
    const scene = Actions.currentScene;
    if (scene === "login" || scene === "repoSelection") {
      BackHandler.exitApp();
      return true;
    }
    Actions.pop();
    return true;
  };

  render() {
    return (
      <Router backAndroidHandler={this._backAndroidHandler} >
        <Stack key="root">
          <Scene key="login" component={LoginScreen} hideNavBar />
          <Scene
            key="repoSelection"
            component={RepoSelectionScreen}
            hideNavBar
          />
          <Scene
            key="commitListing"
            component={CommitListingScreen}
            hideNavBar
          />
        </Stack>
      </Router>
    );
  }
}

export default RouterComponent;
