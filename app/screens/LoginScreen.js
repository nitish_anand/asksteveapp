/* @flow weak */

import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  AsyncStorage
} from "react-native";
import { Actions, ActionConst } from "react-native-router-flux";
import * as Animatable from "react-native-animatable";
import Spinner from "react-native-spinkit";
import { connect } from "react-redux";
import theme from "../themes";
import { Input } from '../components';
import {
  showPasswordField,
  authFormUpdate,
  authenticateGit,
  hidePasswordField
} from "../actions";

class LoginScreen extends Component {
  handleSubmitRef = ref => (this.submit = ref);
  handleErrorRef = ref => (this.error = ref);
  handleChangeUserRef = ref => (this.changeUser = ref);

  async componentWillMount() {
    await AsyncStorage.getItem('AUTHENTICATED').then((token) => {
      if (token === 'true') {
        Actions.repoSelection({ type: ActionConst.RESET });
      }
    })
  }

  onChangeUserPress() {
    this.props.hidePasswordField();
    this.emailRef.fadeInLeft();
    this.changeUser.fadeOut();
    this.passwordRef.fadeOutRight();
    this.error.fadeOut();
  }

  onNextPress() {
    if (this.props.username && this.props.username.indexOf(" ") === -1) {
      this.props.showPasswordField();
      this.emailRef.fadeOutLeft();
      this.changeUser.fadeIn();
      this.passwordRef.fadeInRight();
    } else {
      alert('Please enter valid username');
    }
  }

  onSubmitPress() {
    if (this.props.password && this.props.password.indexOf(" ") === -1) {
      this.submit.transitionTo({ width: 50 })
      this.props.authenticateGit(this.props.username, this.props.password, this.error, this.submit);
    } else {
      alert('Please enter valid password');
    }
  }

  renderActionButton() {
    if (this.props.showPassword) {
      const content = this.props.loading ?
        <Spinner type={"Wave"} color={"#fff"} size={20} /> :
        <Text style={theme.styles.buttonText}>Submit</Text>
      return (
        <TouchableWithoutFeedback onPress={() => this.onSubmitPress()}>
          <Animatable.View
            style={theme.styles.button}
            ref={this.handleSubmitRef}
          >
            {content}
          </Animatable.View>
        </TouchableWithoutFeedback>
      )
    }
    return (
      <TouchableWithoutFeedback onPress={() => this.onNextPress()}>
        <View style={theme.styles.button}>
          <Text style={theme.styles.buttonText}>Next</Text>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="height" style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={theme.images.logo} style={styles.logo} />
        </View>
        <View style={styles.inputContainer}>
          <Animatable.View
          ref={ref => (this.emailRef = ref)}
          style={[styles.inputContent, { zIndex: 2 }]}
        >
          <Input
              placeholder="Enter username"
              value={this.props.username}
              onChangeText={value =>
                this.props.authFormUpdate({
                  prop: 'username',
                  value
                })
              }
              autoCapitalize={'none'}
            />
          </Animatable.View>
          <Animatable.View
            ref={ref => (this.passwordRef = ref)}
            style={[styles.inputContent, { opacity: 0, zIndex: this.props.showPassword ? 2 : 1 }]}
          >
            <Input
              placeholder="Enter password"
              secureTextEntry
              value={this.props.password}
              onChangeText={value =>
                this.props.authFormUpdate({
                  prop: 'password',
                  value
                })
              }
              autoCapitalize={'none'}
            />
          </Animatable.View>
        </View>
        <View>{this.renderActionButton()}</View>
        <Animatable.Text
          ref={this.handleChangeUserRef}
          onPress={() => this.onChangeUserPress()}
          style={[
            theme.styles.buttonText,
            { fontSize: 12, alignSelf: "center", opacity: 0 }
          ]}
        >
          Change Username
        </Animatable.Text>

        <Animatable.Text
          ref={this.handleErrorRef}
          style={[
            theme.styles.buttonText,
            { fontSize: 12, alignSelf: "center", opacity: 0, color: 'red', marginTop: 10 }
          ]}
        >
          Invalid Password
        </Animatable.Text>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    backgroundColor: theme.colors.primaryDark,
  },
  logoContainer: {
    borderRadius: 250,
    width: 250,
    height: 250,
    backgroundColor: '#eee',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },
  inputContainer: {
    position: 'relative',
    height: 50,
    marginTop: 10,
    marginBottom: 10,
    width: '100%'
  },
  inputContent: {
    position: 'absolute',
    width: '100%'
  }
});

const mapStateToProps = ({ auth }) => {
  return {
    showPassword: auth.showPasswordField,
    username: auth.username,
    password: auth.password,
    loading: auth.loading
  };
};

export default connect(
  mapStateToProps,
  { showPasswordField, authFormUpdate, authenticateGit, hidePasswordField }
)(LoginScreen);
