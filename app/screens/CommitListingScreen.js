/* @flow */

import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, Image } from "react-native";
import { connect } from "react-redux";
import theme from '../themes';
import { listRepoCommits } from '../actions';
import Spinner from "react-native-spinkit";
import { ListItem } from '../components';
import Header from '../components/Header';

class CommitListingScreen extends Component {

  onPageEnd() {
    if (this.props.page + 1 <= this.props.last) {
      this.props.listRepoCommits(this.props.name, this.props.page + 1, true)
    }
  }

  _renderListFooter = () => {
    if (this.props.loading) {
      return <Spinner type={"Wave"} color={"#fff"} size={40} style={{ alignSelf: 'center' }} />;
    }
    return null;
  }

  _renderItem = ({ item }) => {
    console.log(item.author);
    return (
      <ListItem
        name={item.committer.login}
        date={item.commit.author.date}
        message={item.commit.message}
        avatar={item.committer.avatar_url}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header />
        <FlatList
          style={{ flexGrow: 1}}
          data={this.props.commits}
          renderItem={this._renderItem}
          onEndReached={this.onPageEnd.bind(this)}
          ListFooterComponent={this._renderListFooter}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: theme.colors.primaryDark,
    paddingTop: 40,
    alignItems: 'center'
  }
});

const mapStateToProps = ({ repo }) => {
  return {
    name: repo.name,
    page: repo.page,
    commits: repo.commits,
    loading: repo.loading,
    last: repo.last
  };
};

export default connect(
  mapStateToProps,
  { listRepoCommits }
)(CommitListingScreen);
