/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableWithoutFeedback
} from "react-native";
import { connect } from "react-redux";
import * as Animatable from "react-native-animatable";
import Spinner from "react-native-spinkit";
import theme from '../themes';
import { Input } from "../components";
import Header from '../components/Header';
import { repoFormUpdate, listRepoCommits } from "../actions";

class RepoSelectionScreen extends Component {
  handleFindRef = ref => (this.find = ref);

  onFindButtonPress() {
    this.find.transitionTo({ width: 50 });
    this.props.listRepoCommits(
      this.props.name,
      this.props.page,
      false,
      this.find
    );
  }

  render() {
    const content = this.props.loading ?
      <Spinner type={"Wave"} color={"#fff"} size={20} /> :
      <Text style={theme.styles.buttonText}>Find Commits</Text>

    return (
      <View style={styles.container}>
        <Header />
        <KeyboardAvoidingView behavior="height" >
          <Input
            placeholder={"Enter repo name"}
            value={this.props.name}
            onChangeText={value =>
              this.props.repoFormUpdate({
                prop: 'name',
                value
              })
            }
          />
          <TouchableWithoutFeedback onPress={() => this.onFindButtonPress()}>
            <Animatable.View
              style={theme.styles.button}
              ref={this.handleFindRef}
            >
              {content}
            </Animatable.View>
          </TouchableWithoutFeedback>
          {
            this.props.loadCommitFail &&
            <Text style={styles.alertText}>Invalid repository, please change and try again </Text>
          }
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: theme.colors.primaryDark,
  },
  alertText: {
    color: 'red',
    width: 300,
    textAlign: 'center',
    marginTop: 20,
    alignSelf: 'center',
    fontFamily: theme.fonts.regular
  }
});

const mapStateToProps = ({ repo }) => {
  return {
    name: repo.name,
    page: repo.page,
    loading: repo.loading,
    loadCommitFail: repo.loadCommitFail
  };
};

export default connect(
  mapStateToProps,
  { repoFormUpdate, listRepoCommits }
)(RepoSelectionScreen);
