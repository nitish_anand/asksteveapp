const colors = {
  primaryDark: '#2b3137'
};

const images = {
  logo: require('./images/octocat.png')
};

const fonts = {
  semibold: 'SpaceGrotesk-SemiBold',
  regular: 'SpaceGrotesk-Regular',
  light: 'SpaceGrotesk-Light',
  bold: 'SpaceGrotesk-Bold',
  medium: 'SpaceGrotesk-Medium'
}

const styles = {
  buttonText: {
    fontFamily: fonts.regular,
    fontSize: 20,
    color: "white"
  },
  button: {
    alignSelf: 'center',
    width: 300,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 25,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default {
  colors,
  images,
  fonts,
  styles
};
