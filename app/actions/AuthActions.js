import { AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import base64 from "base-64";
import axios from 'axios';
import {
  SHOW_PASSWORD_FIELD,
  UPDATE_AUTH_FORM,
  AUTHENTICATING_GITHUB,
  HIDE_PASSWORD_FIELD,
  AUTHENTICATION_GITHUB_FAIL,
  LOGOUT_USER
} from "./types";
import { GIT_BASE_URL } from "../config/config";

export const showPasswordField = () => {
  return {
    type: SHOW_PASSWORD_FIELD
  };
};

export const hidePasswordField = () => {
  return {
    type: HIDE_PASSWORD_FIELD
  };
};

export const authFormUpdate = ({ prop, value }) => {
  return {
    type: UPDATE_AUTH_FORM,
    payload: { prop, value },
  };
};

export const authenticateGit = (username, password, errorText, submitButton) => async dispatch => {
  dispatch ({
    type: AUTHENTICATING_GITHUB
  });
  console.log(username, password);
  console.log(`Basic ${base64.encode(`${username}:${password}`)}`);
  axios({
    url: GIT_BASE_URL,
    method: 'get',
    headers: {
      Authorization: `Basic ${base64.encode(`${username}:${password}`)}`
    }
  }).then(() => {
      AsyncStorage.setItem("AUTHENTICATED", "true");
      Actions.repoSelection();
      submitButton.transitionTo({ width: 300 });
  }).catch(() => {
      dispatch({
        type: AUTHENTICATION_GITHUB_FAIL
      });
      errorText.fadeIn();
      submitButton.transitionTo({ width: 300 });
  })
}

export const logoutUser = () => {
  AsyncStorage.removeItem("AUTHENTICATED");
  Actions.login();
  return {
    type: LOGOUT_USER
  }
}
