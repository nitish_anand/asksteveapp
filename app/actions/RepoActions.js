import { AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import base64 from "base-64";
import axios from "axios";
import parse from "parse-link-header";
import {
  UPDATE_REPO_FORM,
  LIST_REPO_COMMITS,
  LIST_REPO_COMMITS_SUCCESS,
  LIST_REPO_COMMITS_FAIL
} from "./types";
import { GIT_BASE_URL } from "../config/config";
import { parseLinkHeader } from "../utils";

export const repoFormUpdate = ({ prop, value }) => {
  return {
    type: UPDATE_REPO_FORM,
    payload: { prop, value },
  };
};

export const listRepoCommits = (repoName, page, paginate, findButton) => async dispatch => {
  dispatch ({
    type: LIST_REPO_COMMITS
  });
  const author = repoName.split('/')[0];
  const repo = repoName.split('/')[1];
  axios({
    url: `${GIT_BASE_URL}/repos/${author}/${repo}/commits?page=${page}&per_page=10`,
    method: "get"
  }).then(response => {
    const link = parse(response.headers.link);
    dispatch ({
      type: LIST_REPO_COMMITS_SUCCESS,
      payload: { commits: response.data, last: link.last.page, page }
    })
    if (!paginate) {
      Actions.commitListing();
      findButton.transitionTo({ width: 300 });
    }
  }).catch(() => {
    dispatch({
      type: LIST_REPO_COMMITS_FAIL
    });
    findButton.transitionTo({ width: 300 });
  });
}
