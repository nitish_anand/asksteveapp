export const SHOW_PASSWORD_FIELD = "show_password_field";
export const HIDE_PASSWORD_FIELD = 'hide_password_field';
export const UPDATE_AUTH_FORM = "update_auth_form";
export const AUTHENTICATING_GITHUB = "authenticating_github";
export const UPDATE_REPO_FORM = 'update_repo_form';
export const AUTHENTICATION_GITHUB_FAIL = 'authentication_github_fail';
export const LOGOUT_USER = 'logout_user';


export const LIST_REPO_COMMITS = 'list_repo_commits';
export const LIST_REPO_COMMITS_SUCCESS = 'list_repo_commits_success';
export const LIST_REPO_COMMITS_FAIL = 'list_repo_commits_fail';
