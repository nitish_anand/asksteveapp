/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { SafeAreaView } from 'react-native'
import Router from './app/config/routes';
import React from 'react';
import logger from 'redux-logger'
import ReduxThunk from 'redux-thunk';
import reducers from './app/reducers';
import theme from './app/themes'
import {
  View
} from 'react-native';


const store = createStore(reducers, {}, applyMiddleware(ReduxThunk, logger));

const App = () => {
  return (
    <Provider store={store}>
      <SafeAreaView style={{ flex: 1, backgroundColor: theme.colors.primaryDark }}>
        <Router />
      </SafeAreaView>
    </Provider>
  );
};

export default App;
